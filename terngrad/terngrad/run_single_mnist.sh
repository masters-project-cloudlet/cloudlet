#!/bin/bash
set -e
set -x

DATASET_NAME=mnist # imagenet or cifar10 or mnist
ROOT_WORKSPACE=${HOME}/dist_training_results/${DATASET_NAME} # the location to store tf.summary and logs
DATA_DIR=${HOME}/dataset/${DATASET_NAME}-data # dataset location
NUM_GPUS=1 # num of physical gpus
NUM_NODES=1 # num of virtual nodes on physical gpus
OPTIMIZER=momentum
NET=lenet
IMAGE_SIZE=28
BASE_LR=0.01
LR_DECAY_TYPE="polynomial" # learning rate decay type
TRAIN_BATCH_SIZE=64 # total batch size
MAX_STEPS=10000

if [ ! -d "$ROOT_WORKSPACE" ]; then
  echo "${ROOT_WORKSPACE} does not exsit!"
  exit
fi

TRAIN_WORKSPACE=${ROOT_WORKSPACE}/${DATASET_NAME}_training_data/
EVAL_WORKSPACE=${ROOT_WORKSPACE}/${DATASET_NAME}_eval_data/
INFO_WORKSPACE=${ROOT_WORKSPACE}/${DATASET_NAME}_info/
if [ ! -d "${INFO_WORKSPACE}" ]; then
  echo "Creating ${INFO_WORKSPACE} ..."
  mkdir -p ${INFO_WORKSPACE}
fi
current_time=$(date)
current_time=${current_time// /_}
current_time=${current_time//:/-}
FOLDER_NAME=${DATASET_NAME}_${NET}_${NUM_NODES}_${current_time}
TRAIN_DIR=${TRAIN_WORKSPACE}/${FOLDER_NAME}
EVAL_DIR=${EVAL_WORKSPACE}/${FOLDER_NAME}
if [ ! -d "$TRAIN_DIR" ]; then
  echo "Creating ${TRAIN_DIR} ..."
  mkdir -p ${TRAIN_DIR}
fi
if [ ! -d "$EVAL_DIR" ]; then
  echo "Creating ${EVAL_DIR} ..."
  mkdir -p ${EVAL_DIR}
fi


bazel-bin/inception/mnist_train \
--optimizer ${OPTIMIZER} \
--initial_learning_rate ${BASE_LR} \
--learning_rate_decay_type ${LR_DECAY_TYPE} \
--max_steps ${MAX_STEPS} \
--net ${NET} \
--image_size ${IMAGE_SIZE} \
--num_gpus ${NUM_GPUS} \
--batch_size ${TRAIN_BATCH_SIZE} \
--train_dir ${TRAIN_DIR} \
--data_dir ${DATA_DIR} &
