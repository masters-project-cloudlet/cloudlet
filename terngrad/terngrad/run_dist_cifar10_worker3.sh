#!/bin/bash
set -x
set -e
PS=192.168.0.100
WORKER1=192.168.0.100
WORKER2=192.168.0.101
WORKER3=localhost
WORKER4=192.168.0.103

bazel-bin/inception/cifar10_distributed_train \
--optimizer adam \
--initial_learning_rate 0.0002 \
--batch_size 64 \
--num_epochs_per_decay 200 \
--max_steps 300000 \
--seed 123 \
--weight_decay 0.004 \
--net cifar10_alexnet \
--image_size 24 \
--data_dir="$HOME/dataset/cifar10-data-shard-500-749" \
--job_name='worker' \
--task_id=2 \
--ps_hosts="$PS:2222" \
--worker_hosts="${WORKER1}:2224,${WORKER2}:2226,${WORKER3}:2228,${WORKER4}:2230" \
--train_dir=/home/nvidia/dist_training_results/cifar10_case4 &
